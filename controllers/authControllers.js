const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');

module.exports.registration = async (req, res) => {
  const {username, password} = req.body;

  if (!username) {
    return res.status(400).json({message: `Please enter username`});
  }
  if (!password) {
    return res.status(400).json({message: `Please enter password`});
  }

  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });
  console.log(user);

  await user.save();

  res.status(200).json({message: 'User created successfully'});
};

module.exports.login = async (req, res) => {
  const {username, password} = req.body;

  const user = await User.findOne({username});

  if (!user) {
    return res.status(400).json({message: `${username} not found`});
  }
  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({message: 'wrong password'});
  }
  const token = jwt.sign({username: user.username, _id: user._id}, JWT_SECRET);
  console.log(token);
  res.json({message: 'Success', token});
};
