const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string().alphanum().min(3).max(30),

    password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{2,30}$')),
  });
  await schema.validateAsync(req.body);
  next();
};
