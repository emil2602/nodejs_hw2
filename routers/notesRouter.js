const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('./helpers');
const {Note} = require('../models/notesModel');


const {authMiddleware, modelExists} = require('./middleware/authMiddleware');

const {
  addNote,
  getAllNotes,
  getUserNote,
  editUserNote,
  swichCheckUserNote,
  deleteUserNote} = require('../controllers/notesContoller');

router.post('/notes', authMiddleware, asyncWrapper(addNote));
router.get('/notes', authMiddleware, asyncWrapper(getAllNotes));
router.get('/notes/:id', authMiddleware,
    modelExists(Note), asyncWrapper(getUserNote));
router.delete('/notes/:id', authMiddleware,
    modelExists(Note), asyncWrapper(deleteUserNote));
router.put('/notes/:id', authMiddleware,
    modelExists(Note), asyncWrapper(editUserNote));
router.patch('/notes/:id', authMiddleware,
    modelExists(Note), asyncWrapper(swichCheckUserNote));

module.exports = router;
